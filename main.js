document.getElementById("submitButton").addEventListener("click", function () {
  fetch("https://filipstal.pythonanywhere.com/randomqa")
    .then((response) => response.json())
    .then((data) => {
      document.getElementById("questionText").textContent = data.question;
      document.getElementById("answerText").textContent = data.answer;
      document.getElementById("ankiCardText").textContent = data.formatted;
    });
});
